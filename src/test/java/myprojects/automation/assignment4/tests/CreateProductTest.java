package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/*
Дополнительно рассмотрите использование таких аннотаций фреймворка TestNG как @Parameters и @DataProvider.

На основе примеров из лекции создать Maven проект и подключить к нему библиотеки Selenium и TestNG. https://bitbucket.org/qatestlab_automation/lecture-4

Разработать тестовый скрипт, который будет соответствовать следующему сценарию:

Часть А. Создание продукта:
1. Войти в Админ Панель.
2. Выбрать пункт меню Каталог -> повары и дождаться загрузки страницы продуктов.
3. Нажать «Новый товар» для перехода к созданию нового продукта, дождаться загрузки страницы.
4. Заполнить следующие свойства нового продукта: Название продукта, Количество, Цена. Свойства продукта должны генерироваться случайно (случайное название продукта, количество от 1 до 100, цена от 0.1 ₴ до 100 ₴).
5. После заполнения полей активировать продукт используя переключатель на нижней плавающей панели. После активации продукта дождаться всплывающего уведомления о сохранении настроек и закрыть его.
6. Сохранить продукт нажав на кнопку «Сохранить». Дождаться всплывающего уведомления о сохранении настроек и закрыть его.

Часть Б. Проверка отображения продукта:
1. Перейти на главную страницу магазина.
2. Перейти к просмотру всех продуктов воспользовавшись ссылкой «Все товары». Добавить проверку (Assertion), что созданный в Админ Панели продукт отображается на странице.
3. Открыть продукт. Добавить проверки, что название продукта, цена и количество соответствует значениям, которые вводились при создании продукта в первой части сценария.

Настройте выполнение тестового скрипта таким образом, чтобы при вызове выполнения тестов (mvn test) он выполнился на разных браузерах: Chrome, Firefox, Internet Explorer. Для этого можно в testng.xml воспользоваться возможностью передачи параметров.

Дополнительное задание по желанию. Можно подключить ReportNG для генерации удобных отчетов. Для этого воспользуйтесь информацией с официального сайта http://reportng.uncommons.org/ и следующей инструкцией: https://solidsoft.wordpress.com/2011/01/23/better-looking-html-test-reports-for-
testng-with-reportng-maven-guide/

Примечания:
1. Логику сценария можно разбить на пару методов @Test и настроить цепочку выполнения используя атрибут dependsOnMethods для данной аннотации.
2. Тестовый скрипт должен содержать проверки (assertions) и дополнительный лог действий, который можно увидеть в отчете о прогоне тестов. Для удобства логирование некоторых действий можно вынести в свою реализацию слушателя WebDriverEventListener, речь о котором шла на прошлой лекции.
3. @DataProvider предназначен для передачи тестовых данных в скрипты. Используйте данную аннотацию для передачи логина и пароля в процессе авторизации.
4. @Parameters позволяет передавать параметры из testng.xml. Используйте данную аннотацию для определения того, какой тип драйвера необходимо поднять для теста (Chrome, Firefox или же Internet Explorer.)

Для доступа в Админ Панель используйте следующие данные:
Адрес: http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/
Логин: webinar.test@gmail.com
Пароль: Xcg7299bnSmMuRLp9ITw
Магазин доступен по адресу http://prestashop-automation.qatestlab.com.ua/
 */


public class CreateProductTest extends BaseTest {

    @DataProvider
    public Object[][] testData() {
        return new Object[][] {
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"},
        };
    }

    private final ProductData productData = ProductData.generate();

    @Test(dataProvider = "testData")
    public void createNewProduct(String login, String password) {
        actions.login(login, password);
        actions.createProduct(productData);
    }

    @Test(dependsOnMethods = "createNewProduct")
    public void checkNewProduct(){
        actions.searchProduct(productData);
    }

}
