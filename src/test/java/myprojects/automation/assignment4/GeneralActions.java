package myprojects.automation.assignment4;


import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private JavascriptExecutor js;
    private Actions actions;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        js = (JavascriptExecutor)driver;
        actions = new Actions(driver);
    }

    /**
     * Logs in to Admin Panel.
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        driver.navigate().to(Properties.getBaseAdminUrl());

        By loginFieldLocator = By.xpath("//input[@id='email']");

        wait.until(ExpectedConditions.presenceOfElementLocated(loginFieldLocator));

        WebElement loginField = driver.findElement(loginFieldLocator);
        loginField.clear();
        loginField.sendKeys(login);

        WebElement passwordField = driver.findElement(By.xpath("//input[@id='passwd']"));
        passwordField.clear();
        passwordField.sendKeys(password);

        WebElement loginBtn = driver.findElement(By.xpath("//button[@name='submitLogin']"));
        loginBtn.submit();

        waitForContentLoad();
    }

    public void createProduct(ProductData newProduct) {
        WebElement ordersBtn = driver.findElement(By.xpath("//li[@id='subtab-AdminCatalog']"));
        actions.moveToElement(ordersBtn).build().perform();

        WebElement productsBtn = scrollTo(By.xpath("//li[@id='subtab-AdminProducts']"));
        productsBtn.click();

        WebElement newProductBtn = scrollTo(By.xpath("//a[@id='page-header-desc-configuration-add']"));
        newProductBtn.click();


        //fill product fields
        WebElement productNameField = scrollTo(By.xpath("//input[@id='form_step1_name_1']"));
        productNameField.clear();
        productNameField.sendKeys(newProduct.getName());

        WebElement productQuantityField = scrollTo(By.xpath("//input[@id='form_step1_qty_0_shortcut']"));
        productQuantityField.clear();
        productQuantityField.sendKeys(String.valueOf(newProduct.getQty()));

        WebElement productPriceField = scrollTo(By.xpath("//input[@id='form_step1_price_shortcut']"));
        productPriceField.clear();
        productPriceField.sendKeys(newProduct.getPrice());

        //activate

        WebElement activationInput = scrollTo(By.xpath("//div[@class='switch-input ']"));
        activationInput.click();

        closeMsg();

        WebElement saveBtn = scrollTo(By.xpath("//button[@class='btn btn-primary js-btn-save']"));
        saveBtn.submit();

        closeMsg();
    }

    private void closeMsg() {
        By messageCloseLocator = By.xpath("//div[@class='growl-close']");

        wait.until(ExpectedConditions.visibilityOfElementLocated(messageCloseLocator));
        WebElement closeMeg = driver.findElement(messageCloseLocator);
        closeMeg.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(messageCloseLocator));
    }

    private WebElement scrollTo(By locator){
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        js.executeScript("arguments[0].scrollIntoView(true);",element);
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public void searchProduct(ProductData productData){
        driver.navigate().to(Properties.getBaseUrl());

        WebElement allProductsBtn = scrollTo(By.xpath("//a[@class='all-product-link pull-xs-left pull-md-right h4']"));
        allProductsBtn.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[@class='h6 active-filter-title']")));

        WebElement productElement = getOurProduct(productData);

        Assert.assertNotNull(productElement, "Can't find product["+productData.getName()+"] from all products");

        productElement.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='product-add-to-cart']")));

        WebElement nameElement = driver.findElement(By.xpath("//h1[@class='h1']"));

        Assert.assertEquals(nameElement.getText().toUpperCase(), productData.getName().toUpperCase(), "Name:");

        WebElement priceElement = driver.findElement(By.xpath("//div[@class='current-price']/span"));
        String priceString = priceElement.getAttribute("content");
        double price = Double.valueOf(priceString);
        double expPrice = Double.valueOf(productData.getPrice().replace(",", "."));

        Assert.assertEquals(price, expPrice, "Price:");

        WebElement productDetails = scrollTo(By.xpath("//div[@id='product-details']"));
        productDetails.click();

        WebElement qtyElement = productDetails.findElement(By.xpath("./div[@class='product-quantities']/span"));
        String qtyString = qtyElement.getText();
        qtyString = qtyString.split(" ")[0];

        Assert.assertEquals(Integer.valueOf(qtyString), productData.getQty(), "Quantity:");
    }

    private WebElement getOurProduct(ProductData productData){

        String sMainLocator = "//div[@class='products row']/article";

        String sSubLocator = "/div/div/h1";

        By locator = By.xpath(sMainLocator);

        scrollTo(locator);

        List<WebElement> productsList = driver.findElements(locator);

        for (int i = 1; i <= productsList.size(); i++){
            String subLocator = sMainLocator + "["+i+"]" + sSubLocator;
            WebElement subElement = scrollTo(By.xpath(subLocator));
            String text = subElement.getText();
            if(text.equals(productData.getName())){
                return subElement;
            }
        }

        /*Если на первой странице не нашлось ннашего продукта, пытаемся перейти на следующую страницу
        и продолжаем поиск
         */

        By firstCardLocator = By.xpath(sMainLocator + sSubLocator);
        WebElement firsCard = scrollTo(firstCardLocator);

        WebElement nextBtn = scrollTo(By.xpath("//a[@rel='next']"));
        String className = nextBtn.getAttribute("class");
        if(className.contains("disabled")){ //пробовал выполнить !nextBtn.isEnabled() но условие не срабатывало. Пришлось завязываться на изменение в классе
            return null;
        } else {
            nextBtn.click();
            wait.until(webDriver -> !firsCard.equals(driver.findElement(firstCardLocator))); //ожидаем что первый элемент таблицы - измениться. Без него в рекурсии получал всегда одинаковые элементы.
            return getOurProduct(productData);
        }
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad() {
        wait.until(ExpectedConditions
                .attributeContains(By.xpath("//*[@id='ajax_running']"), "style", "display: none;"));
    }
}
